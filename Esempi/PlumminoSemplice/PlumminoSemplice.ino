/* (atrent)
 * Plummino semplificato (senza task)
 */

#define DEBUG

#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>
#include <PubSubClientTools.h>
#define MQTT_SERVER "mqtt.atrent.it"
WiFiClient espClient;
PubSubClient client(MQTT_SERVER, 1883, espClient);
PubSubClientTools mqtt(client);


/* Pass as a parameter the trigger and echo pin, respectively,
 * or only the signal pin (for sensors 3 pins), like:
 * Ultrasonic ultrasonic(13); */
#include <Ultrasonic.h>
Ultrasonic ultrasonic(D6, D5, 10000UL);
int obstacleDistance;
//#define MAXDISTANCE 50

#include "WEMOS_Motor.h"
long hold=0;
//String command="";
#define MOTORS_CYCLE 100

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
/* Assign a unique ID to this sensor at the same time */
#define HMC5883_ADDRESS_MAG 0x1E
Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(999);
sensor_t sensor;
sensors_event_t event;
int headingDegrees;
//void compassDetails();
#define XCORRECTION 1
#define YCORRECTION 20

// Accelerometer
const int GY521=0x68; // I2C address
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

// bumpers
#define COLLISION_LEFT D0
#define COLLISION_RIGHT D3
int collision_left,collision_right;

// switch
#define SWITCHPROG A0
int switchprog;

// target
int courseToGo=252;
int error;

//Motor shield I2C Address: 0x30
//PWM frequency: 1000Hz(1kHz)
Motor *Mright = NULL; // right
Motor *Mleft  = NULL; // left

// motors
float rudder;
//float speed;
int speedLeft, speedRight;
int rotLeft, rotRight;

//#define ZEROING_TIME 10000
#define SENSITIVITY 0.1
#define ACCEL_SENSITIVITY 2
#define SILENCE		500

//#define TOP_SPEED_R	70
//#define TOP_SPEED_L	70

#define TOP_SPEED_R	56
#define MIN_SPEED_R	17

#define TOP_SPEED_L	50
#define MIN_SPEED_L	15

const byte MAX_MSG_SIZE PROGMEM=100;
byte packetBuffer[MAX_MSG_SIZE];  //buffer to hold incoming udp packet

//#include "wifi.h"
const String ssid = "SistEmbed";
const String pass = "8caratteri8"; // INSERIRE PASSWORD
const boolean STA PROGMEM=true;
//const boolean STA PROGMEM=false; // così fa da AP
WiFiUDP Udp;

/////////////////////////////////////////////////////
void setup() {
    pinMode(LED_BUILTIN,OUTPUT);
    collisionReset();

    Serial.begin(115200);
    Serial.println("Setup...");

    setupWifi();

    setupOTA();

    setupMQTT();

    setupI2C();

    Mright = new Motor(0x30,_MOTOR_A, 1000);//Motor A
    Mleft  = new Motor(0x30,_MOTOR_B, 1000);//Motor B

    /* Initialise the compass */
    if(!mag.begin()) {
        /* There was a problem detecting the HMC5883 ... check your connections */
        Serial.println("Ooops, no HMC5883 detected ... Check your wiring!");
        while(1);
    }
    /* Display some basic information on this sensor */
    //compassDetails();

    Serial.println("End of setup...");
}

void loop() {
    readSerial();
    readSensors();
    strategy();
    ArduinoOTA.handle();
    pingLed();
    serialStatus();
    mqttStatus();
    delay(500);
}

void pingLed() {
    digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)
    delay(10);
    digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
}

void readSensors() {
    // COMPASS
    //Serial.println("pre bussola");
    mag.getSensor(&sensor);
    mag.getEvent(&event);
    //Serial.println("post bussola");

    // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
    // Calculate heading when the magnetometer is level, then correct for signs of axis.
    float heading = atan2(event.magnetic.y+YCORRECTION, event.magnetic.x+XCORRECTION);

    // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
    // Find yours here: http://www.magnetic-declination.com/
    // Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
    // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
    float declinationAngle = 0.04; // corretto per milano
    heading += declinationAngle;

    // Correct for when signs are reversed.
    if(heading < 0)
        heading += 2*PI;

    // Check for wrap due to addition of declination.
    if(heading > 2*PI)
        heading -= 2*PI;

    // Convert radians to degrees for readability.
    headingDegrees =  (int)(heading * 180/M_PI);

    ////////////////////////////////////////////
    switchprog=analogRead(SWITCHPROG);

    ////////////////////////////////////////////
    // Collision
    collision_right=digitalRead(COLLISION_RIGHT);
    collision_left=digitalRead(COLLISION_LEFT);
    collisionReset();

    ////////////////////////////////////////////
    // Distance
    obstacleDistance=ultrasonic.distanceRead();
    //if(distance>MAXDISTANCE) distance=MAXDISTANCE;
    //Serial.println(distance);

    // call gps
    //callbackGPS();

    ////////////////////////////////////////////
    // Accelerometro
    readAccelerometer();
}

void mqttStatus() {
    mqtt.publish("SistEmbed/plummino/heading", String(headingDegrees));
}

void serialStatus() {
    Serial.println("---OTA---------------------------------");

    Serial.print("Sonar distance: ");
    Serial.println(obstacleDistance);

    Serial.print("Collision: left ");
    Serial.print(collision_left);
    Serial.print(" right ");
    Serial.println(collision_right);

    Serial.print("Switch: ");
    Serial.println(switchprog);

    /*
    Serial.print("GPS sentences: ");
    Serial.print(nrOfSentences);
    Serial.print(",");
    Serial.println(nmea.getSentence());
    */

    Serial.print("Accelerometer: (X=");
    Serial.print(AcX);
    Serial.print(", Y=");
    Serial.print(AcY);
    Serial.print(", Z=");
    Serial.print(AcZ);
    Serial.print(") - T=");
    Serial.print(Tmp);
    Serial.print("- (GX=");
    Serial.print(GyX);
    Serial.print(", GY=");
    Serial.print(GyY);
    Serial.print(", GZ=");
    Serial.print(GyZ);
    Serial.println(")");

    printCompassDetails();

    /*
    Serial.print("Last command: ");
    Serial.print(command);
    Serial.print("\tfor ");
    Serial.println(hold);
    */

    Serial.print("Speed: L=");
    Serial.print(speedLeft);
    Serial.print(", R=");
    Serial.println(speedRight);

    Serial.print("Rot: L=");
    Serial.print(rotLeft);
    Serial.print(", R=");
    Serial.println(rotRight);

    Serial.print("Err: ");
    Serial.println(error);

    Serial.print("course2go: ");
    Serial.println(courseToGo);
}

void strategy() {

    // TODO implementare correzione >180

    error=courseToGo-headingDegrees;

    if(error>0) {
        rotLeft=_CCW;
        rotRight=_CW;
    } else {
        rotRight=_CCW;
        rotLeft=_CW;
    }

    int err=abs(error);
    // perché info sulla direzione già inclusa in rotLeft/rotRight
    speedLeft=map(err,0,359,MIN_SPEED_L,TOP_SPEED_L);
    speedRight=map(err,0,359,MIN_SPEED_R,TOP_SPEED_R);

    // con vero PID non servirebbe?
    if(err>5)
        setMotors();
    else
        fullStop();
}

void setupWifi() {
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid.c_str(), pass.c_str());

    Serial.print("Connecting...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println();

    Serial.print("Connected, IP address: ");
    Serial.println(WiFi.localIP());
}

void printCompassDetails() {
    //mag.getSensor(&sensor);
    /*
    Serial.println("------------------------------------");
    Serial.print  ("Sensor:       ");
    Serial.print(sensor.name);
    Serial.print  ("Driver Ver:   ");
    Serial.println(sensor.version);
    Serial.print  ("Unique ID:    ");
    Serial.println(sensor.sensor_id);
    Serial.print  ("Max Value:    ");
    Serial.print(sensor.max_value);
    Serial.println(" uT");
    Serial.print  ("Min Value:    ");
    Serial.print(sensor.min_value);
    Serial.println(" uT");
    Serial.print  ("Resolution:   ");
    Serial.print(sensor.resolution);
    Serial.println(" uT");
    Serial.println("------------------------------------");
    */
    Serial.print("Heading: ");
    Serial.print(headingDegrees);
    Serial.print("\t(X: ");
    Serial.print(event.magnetic.x);
    Serial.print("  ");
    Serial.print("Y: ");
    Serial.print(event.magnetic.y);
    Serial.print("  ");
    Serial.print("Z: ");
    Serial.print(event.magnetic.z);
    Serial.print("  ");
    Serial.println("uT)");
    //Serial.println("------------------------------------");
}

void collisionReset() {
    // strano dover fare 'sta cosa, se no non tornano su, mah!
    // DOVUTO A MANCATO SUPPORTO PULLUP SU ALCUNI GPIO DEL WEMOS
    // (mia lacuna ai tempi, qualche anno fa)
    pinMode(COLLISION_LEFT,OUTPUT);
    pinMode(COLLISION_RIGHT,OUTPUT);
    digitalWrite(COLLISION_LEFT,HIGH);
    digitalWrite(COLLISION_RIGHT,HIGH);
    pinMode(COLLISION_LEFT,INPUT_PULLUP);
    pinMode(COLLISION_RIGHT,INPUT_PULLUP);
    // tra l'altro solo il left, ri-mah! INFATTI solo quello non supporta PULLUP
}

// TODO trovare lib che faccia un po' di smoothing e integrazione
// NON trovata... :(
void readAccelerometer() {
    // mando il messaggio di richiesta valori
    Wire.beginTransmission(GY521);
    Wire.write(0x3B);  // a partire dal registro 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission(false);
    Wire.requestFrom(GY521,14,true);  // in tutto 14 registri
    AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}


void setMotors() {
    if(switchprog>1000) {
        Mright->setmotor(rotRight, speedRight);
        Mleft->setmotor(rotLeft, speedLeft);
    } else
        fullStop();
}

void fullStop() {
    Mright->setmotor(_STOP);
    Mleft->setmotor(_STOP);
}



void setupOTA() {
    // REMEMBER! https://github.com/esp8266/Arduino/issues/2480

    // Port defaults to 8266
    // ArduinoOTA.setPort(8266);

    // Hostname defaults to esp8266-[ChipID]
    ArduinoOTA.setHostname("Plummino");

    // No authentication by default
    //ArduinoOTA.setPassword("plum");

    // Password can be set with it's md5 value as well
    // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
    // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

    /* avanzi OTA
      // Port defaults to 8266
      // ArduinoOTA.setPort(8266);

      // Hostname defaults to esp8266-[ChipID]
      // ArduinoOTA.setHostname("myesp8266");

      // No authentication by default
      // ArduinoOTA.setPassword((const char *)"123");
    */

    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
            type = "sketch";
        } else { // U_FS
            type = "filesystem";
        }

        // NOTE: if updating FS this would be the place to unmount FS using FS.end()
        Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
    });

    ArduinoOTA.begin();
    Serial.println("OTA Ready");
}



void setupMQTT() {
    // Connect to MQTT
    Serial.print("Connecting to MQTT: "+String(MQTT_SERVER)+" ... ");
    if (client.connect("PlumminoSemplice")) {
        Serial.println("connected");
        /*
        mqtt.subscribe("SistEmbed/plummin/imposta",    blink);
        mqtt.subscribe("#",        monitor);
        mqtt.subscribe("SistEmbed/fdossena/bright", luminosita);
        */

    } else {
        Serial.println("Failed, rc="+client.state());
    }

}

void setupI2C() {
    //setup ethernet part
    Udp.begin(7400);

    Wire.begin();
    // mando il messaggio di accensione
    Wire.beginTransmission(GY521);
    Wire.write(0x6B);  // registro PWR_MGMT_1
    Wire.write(0);     // se zero sveglia il chip MPU-6050
    Wire.endTransmission(true);
}

void readSerial() {
    if(Serial.available()) {
        //command=Serial.readStringUntil(' ');
        courseToGo=Serial.parseInt();
        while(Serial.available()) {
            Serial.read();
        };
        delay(5000);
    }
}


/* avanzi motori
  M1->setmotor(_STOP);
  M2->setmotor( _STOP);

  M1->setmotor(_SHORT_BRAKE);
  M2->setmotor( _SHORT_BRAKE);

  M1->setmotor(_STANDBY);//Both Motor standby
  M2.setmotor( _STANDBY);
*/


/*
void setRLfromRudder() {
    speedRight = map(rudder, 10, -10, 1, TOP_SPEED_R);
    speedLeft = map(rudder, -10, 10, 1, TOP_SPEED_L);
}
*/
